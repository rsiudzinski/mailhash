<?php

namespace Drupal\hash_mail\Entity;

class HashMailUser extends \Drupal\user\Entity\User {
  public ?string $unhashed_email = NULL;
}
