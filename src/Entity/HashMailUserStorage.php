<?php

namespace Drupal\hash_mail\Entity;

use Drupal\user\UserStorage;

class HashMailUserStorage extends UserStorage {

  /**
   * @inerhitDoc
   */
  public function loadByProperties(array $values = []) {
    // Überprüfe, ob nach der E-Mail gesucht wird.
    if (array_key_exists('mail', $values)) {
      // Merke die ungehashte E-Mail-Adresse.
      $unhashed_mail = $values['mail'];
      // Ersetze die ungehashte Adresse durch die gehashte Adresse
      $values['mail'] = \Drupal::service('hash_mail.hash')->hash_mail($unhashed_mail);
      // Lade die User-Objekte.
      // Da die E-Mail-Adresse eine ID ist, sollten keine oder eine Adresse zurückkommen.
      // Diese kommen in einem Array zurück.
      $users = parent::loadByProperties($values);
      // Sollte nicht oder nur einmal durchlaufen.
      foreach ($users as $user) {
        // Fügt die ungehashte E-Mail-Adresse an das User-Objekt an.
        $user->unhashed_email = $unhashed_mail;
      }
      return $users;
    } else {
      // Falls nicht nach der E-Mail-Adresse gesucht wird, reiche an Eltern-Funktion durch.
      return parent::loadByProperties($values);
    }
  }
}
