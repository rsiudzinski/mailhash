<?php

namespace Drupal\hash_mail\Constraint;

use Symfony\Component\Validator\Constraint;

class HashMailProtectedUserFieldConstraintValidator extends \Drupal\user\Plugin\Validation\Constraint\ProtectedUserFieldConstraintValidator {

  /**
   * @inerhitDoc
   */
  public function validate($items, Constraint $constraint) {
    if (!isset($items)) {
      return;
    }
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    $field = $items->getFieldDefinition();

    // Special case for the password, it being empty means that the existing
    // password should not be changed, ignore empty password fields.
    $value = $items->value;
    if ($field->getName() != 'mail' || !empty($value)) {
      parent::validate($items, $constraint);
    }
  }

}
