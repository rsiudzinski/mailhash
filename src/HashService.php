<?php

namespace Drupal\hash_mail;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class HashService {

  const HASH_COUNT = 15;
  const EMAIL_MAX_LENGTH = 320;

  /**
   * @param $mail string
   *
   * @return string
   *
   * Diese Funktion ist eine Abwandlung von \Drupal\Core\Password\PhpassHashedPassword::crypt
   */
  public function hash_mail($mail, $algo="sha256") {
    // Prevent DoS attacks by refusing to hash large emails.
    if (strlen($mail) > HashService::EMAIL_MAX_LENGTH)
      throw new BadRequestHttpException();

    $pepper = Settings::getHashSalt();
    // Set the counter to 2^HASH_COUNT
    $count = 1 << HashService::HASH_COUNT;

    //                                  Binary
    $hash = hash($algo, $pepper . $mail, TRUE);
    do {
      //                                  Binary
      $hash = hash($algo, $hash . $mail, TRUE);
    } while (--$count);

    return base64_encode($hash);
  }

  /**
   * Conditionally create and send a notification email when a certain
   * operation happens on the given user account.
   *
   * Must be used instead of _user_mail_notify if the E-Mail Address is hashed.
   *
   * @param string $op
   *   The operation being performed on the account. Possible values:
   *   - 'register_admin_created': Welcome message for user created by the admin.
   *   - 'register_no_approval_required': Welcome message when user
   *     self-registers.
   *   - 'register_pending_approval': Welcome message, user pending admin
   *     approval.
   *   - 'password_reset': Password recovery request.
   *   - 'status_activated': Account activated.
   *   - 'status_blocked': Account blocked.
   *   - 'cancel_confirm': Account cancellation request.
   *   - 'status_canceled': Account canceled.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user object of the account being notified. Must contain at
   *   least the fields 'uid', 'name', and 'mail'.
   *
   * @param string $email_address
   *   The E-Mail Address of the user.
   *   As the database entry will only have the Hash of the E-Mail, the Address
   *   itself must be provided.
   *
   * @return array
   *   An array containing various information about the message.
   *   See \Drupal\Core\Mail\MailManagerInterface::mail() for details.
   *
   * @see user_mail_tokens()
   */
  public function user_mail_notify($op, AccountInterface $account, string $email_address) {

    if (
      \Drupal::config('user.settings')->get('notify.' . $op) &&
      $this->hash_mail($email_address) === $account->getEmail()
    ) {
      $params['account'] = $account;
      $langcode = $account->getPreferredLangcode();
      // Get the custom site notification email to use as the from email address
      // if it has been set.
      $site_mail = \Drupal::config('system.site')->get('mail_notification');
      // If the custom site notification email has not been set, we use the site
      // default for this.
      if (empty($site_mail)) {
        $site_mail = \Drupal::config('system.site')->get('mail');
      }
      if (empty($site_mail)) {
        $site_mail = ini_get('sendmail_from');
      }
      $mail = \Drupal::service('plugin.manager.mail')->mail('user', $op, $email_address, $langcode, $params, $site_mail);
      if ($op == 'register_pending_approval') {
        // If a user registered requiring admin approval, notify the admin, too.
        // We use the site default language for this.
        \Drupal::service('plugin.manager.mail')->mail('user', 'register_pending_approval_admin', $site_mail, \Drupal::languageManager()->getDefaultLanguage()->getId(), $params);
      }
    }
    return empty($mail) ? NULL : $mail['result'];
  }
}
